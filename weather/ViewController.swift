//
//  ViewController.swift
//  weather
//
//  Created by Antonio Pertusa on 18/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, XMLParserDelegate, ConnectionDelegate {
    
    @IBOutlet weak var cityName: UITextField!
    @IBOutlet weak var segmentedControl: UISegmentedControl!

    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var windSpeed: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    
    @IBOutlet weak var icon: UIImageView!
    
    // Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cityName.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.cityName.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func floatToString(_ value: Float?) -> String
    {
        if let value = value {
            return "\(value)"
        }
        return "--"
    }
    
    func updateView(weather : Weather)
    {
        self.humidity.text = self.floatToString(weather.humidity)
        self.temperature.text = self.floatToString(weather.temperature)
        self.windSpeed.text = self.floatToString(weather.windSpeed)
        self.weatherDescription.text = weather.description
        self.country.text = weather.country
        
        // Descargar y actualizar la imagen aquí
        let session = URLSession(configuration: .default)
        if let url = URL(string:weather.imageURL!) {
            session.dataTask( with:url, completionHandler: { data, response, error in
                if error == nil,
                    let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200 {
                        DispatchQueue.main.async {
                            self.icon.image = UIImage(data: data)
                        }
                    }
                }).resume()
        }
    }


    func parseJSON(_ data: Data) {
        // Guardar data como un Dictionary
        if let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) as! [String:Any] {
            // Mostrarlo por consola con print. () significa array, {} es diccionario
            print(jsonData)
            // Llamar al constructor de weather para extraer la información
            let weather = Weather(json: jsonData)
            // Llamar a updateView para actualizar las etiquetas correspondientes
            updateView(weather: weather!)
        }
    }
    
    func parseXML(_ data: Data) {
        // Inicializar el parser. La extracción se hará en los métodos delegados de XMLParserDelegate
        // En este caso, se creará un objeto de clase Weather y se escribirá en sus variables públicas conforme se va leyendo el XML, como puede verse abajo.
        let parser = XMLParser(data: data)
        parser.delegate = self
        let result = parser.parse()
        if result {
            print("todo correcto")
        } else {
            print("parse XML error")
        }
    }
    
    
    @IBAction func search(_ sender: Any) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        let type = self.segmentedControl.titleForSegment(at: self.segmentedControl.selectedSegmentIndex)
        
        let apiKey = "ba01089290fe01d324a7bd5dcfb5a603"
        let urlString : String?
        
        if type == "JSON" {
            urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(self.cityName.text!)&APPID=\(apiKey)"
        }
        else {
            urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(self.cityName.text!)&mode=xml&APPID=\(apiKey)"
        }

        // Se debe inicializar la conexión aquí
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        let connection = Connection(delegate: self)
        connection.startConnection(session, with: urlString!)
    }
    
    func connectionSucceed(_ connection: Connection, with data: Data) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        // Para debugging
        if let contents = String(data: data, encoding : .utf8) {
            print ("Received: \(contents)")
        }
        
        if self.segmentedControl.titleForSegment(at: self.segmentedControl.selectedSegmentIndex) == "XML" {
            self.parseXML(data)
        }
        else {
            self.parseJSON(data)
        }
        
        // URL de ejemplo para un icono: http://openweathermap.org/img/w/10d.png
    }
    
    func connectionFailed(_ connection: Connection, with error: String) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        print(error)
    }
    
    // MARK: - XML Parsing
    private var currentMessage : String?
    private var weather = Weather()!
    
    func parserDidStartDocument(_ parser: XMLParser) {
        self.weather = Weather()! // Creamos un objeto weather con valores por defecto
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        self.updateView(weather: self.weather) // Actualizamos la vista cuando el parser haya terminado
    }

    // Falta añadir aquí los métodos delegados didStartElement, didEndElement y foundCharacters
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        
        switch elementName.lowercased() {
        case "speed":
            weather.windSpeed = Float.init(attributeDict["value"]!)
        case "temperature":
            weather.temperature = Float.init(attributeDict["value"]!)
        case "weather":
            weather.description = attributeDict["value"]
            weather.setIcon(attributeDict["icon"])
        case "humidity":
            weather.humidity = Float.init(attributeDict["value"]!)
        default:
            break
        }
     }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let trimmedString = string.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if trimmedString != "" {
            self.weather.country = trimmedString
        }
    }
}

