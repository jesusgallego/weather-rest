//
//  Weather.swift
//  weather
//
//  Created by Antonio Pertusa on 19/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import Foundation

class Weather {
    var humidity : Float?
    var temperature : Float?
    var windSpeed : Float?
    var description : String?
    var country : String?
    var imageURL : String?
    
    init?(json: [String: Any]) {
        // Crear el objeto a partir del diccionario recibido
        
        guard let main = json["main"] as? [String:Float],
            let wind = json["wind"] as? [String:Any],
            let sys = json["sys"] as? [String:Any],
            let weather = json["weather"] as? [Any],
            let humidity = main["humidity"],
            let temperature = main["temp"],
            let windSpeed = wind["speed"] as? Float,
            let country = sys["country"] as? String
            else {
                return nil
        }
        
        for object in weather {
            if let obj = object as? [String:Any],
                let description = obj["description"] as? String,
                let icon = obj["icon"] as? String {
                
                self.humidity = humidity
                self.temperature = temperature
                self.windSpeed = windSpeed
                self.description = description
                self.country = country
                self.setIcon(icon)
                
            } else{
                return nil
            }
        }
        
        
    }
    
    init?() {
        // No hacer nada
    }
    
    public func setIcon(_ icon: String?) {
        if let imgIcon = icon {
            self.imageURL = "http://openweathermap.org/img/w/\(imgIcon).png"
        }
    }
}
